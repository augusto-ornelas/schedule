#define _XOPEN_SOURCE
#define _POSIX_C_SOURCE 201809L
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <dirent.h>
#define TASKS 150000
#define TODOLIST_SIZE 10000
#define DATE_STR_SIZE 35

enum { OPEN, DONE };
// TODO: The stats that I
// Grouped by time
// Grouped by task
// TODO: I need a way to count the total hours I should work
// 7 hours per day only on weekdays
// Select a period of time to give stats
// I get an array of worked per day divided by task hours per day

struct task {
	time_t time; // time spent in the task in seconds
	struct tm start;
	struct tm end;
	char *name;
	char *description;
};

// TODO: implement todo functionality
struct TodoItem {
	char *description;
	unsigned int task_id;
	int status; // in progres b
};

bool scratch = false;
int i = 0;

// enough for 20 years of tasks, 20 tasks per day, 365 days per year
// NOTE: 22MB Just to start
// TODO: let's try to implement with a dynamic array and see the difference
static struct task tasks[TASKS] = { 0 };
static struct TodoItem todos[TODOLIST_SIZE] = { 0 };
int todo_index = 0;

static char *time_format = "%a %b %d %I:%M:%S %p %Z %Y";

int handle_cmd_line(char *line, int nread, int *i, time_t *start, time_t *end);
void *checked_realloc(void *p, size_t size);
void duration(char *buf, time_t seconds);
int get_operation(char **args, char *buf);
time_t mktime_or_exit(struct tm *t);
void print_tm(struct tm *t);
char *str_to_tm(struct tm *t, char *str);
void load_file(FILE *file);
void write_to_file(const char *filename, char *command, char *args);
char *status_to_str(int status);

// In script mode use the file as a script don't use interactive
#define SCRIPT_MODE_FLAG 0 // 0b00000000
#define INTERACTIVE_MODE_FLAG 1 // 0b00000001
#define SCRATCH_MODE_FLAG 2 // 0b00000010
// Possible combinations interactive and scratch
// script: no interactive and scratch

uint8_t mode = INTERACTIVE_MODE_FLAG;
static const char *usage = "%s: [-S|--scratch] [-s] [-f <filename>]\n";

#ifdef TEST
#include <assert.h>
int main(int argc, char *argv[])
{
	// Test duration string
	printf("size of task item: %lu\n", sizeof(struct task));
	printf("size of tm: %lu\n", sizeof(struct tm));
	printf("size of time_t: %lu\n", sizeof(time_t));
	printf("size of char *: %lu\n", sizeof(char *));
	printf("Testing\n");
	char buf[30];
	duration(buf, 5715);
	buf[29] = '\0';
	assert(strcmp(buf, "1h 35m 15s") == 0);
	printf("PASSED\n");
	// test get operation
	// char line[] = "start <time>";
	// char *args = NULL int get_operation() return 0;
	// Check how to get a tm from now
	// TODO add tests with edge cases for all functions
	printf("now in localtime\n");
	time_t now = time(NULL);
	struct tm *my_tm = localtime(&now);
	printf("now = ");
	print_tm(my_tm);
	char date[DATE_STR_SIZE];
	int date_len = strftime(date, DATE_STR_SIZE, time_format, my_tm);
	if (date_len == 0) {
		fprintf(stderr, "%d is not enough space\n", DATE_STR_SIZE);
		exit(1);
	}
	printf("the time right now is %s\n", date);
	memset(my_tm, 0, sizeof(*my_tm));
	char *date_str = "Sun Mar 26 04:37:02 PM CEST 2023";
	char *ok = strptime(date_str, time_format, my_tm);
	assert(ok != NULL);
	printf("After parsing with strptime\n");
	print_tm(my_tm);
	printf("After parsing\n");
	time_t t = mktime(my_tm);
	printf("After calling mktime\n");
	print_tm(my_tm);
	printf("print using strftime\n");
	char start_date[DATE_STR_SIZE];
	date_len = strftime(start_date, DATE_STR_SIZE, time_format, my_tm);
	printf("%s\n", start_date);
	printf("Let's try another date format rfc-3339\n");
	date_str = "2023-03-26 20:37:03+02:00";
	memset(my_tm, 0, sizeof(*my_tm));
	ok = strptime(date_str, "%Y-%m-%d %H:%M:%S%z", my_tm);
	my_tm->tm_isdst = -1;
	assert(ok != NULL);
	printf("After parsing with strptime\n");
	print_tm(my_tm);
	printf("After parsing\n");
	t = mktime(my_tm);
	printf("After calling mktime\n");
	print_tm(my_tm);
	printf("print using strftime\n");
	date_len = strftime(start_date, DATE_STR_SIZE, time_format, my_tm);
	printf("%s\n", start_date);
	int year, month, day;
	printf("insert a date YYYY-mm-dd: ");
	fflush(stdout);

	int ret = sscanf("2022-3", "%d-%d-%d", &year, &month, &day);
	printf("ret = %d\n", ret);
	if (ret != 3) {
		printf("ERROR: unable to parse date\n");
	} else {
		printf("%d-%d-%d\n", year, month, day);
	}
	return 0;
}
#else // END TEST
int main(int argc, char *argv[])
{
	time_t now;
	// DONE: add a flag to not override the file: --scratch or something similar
	// DONE: Add a flag for the file -f
	// DONE: Add a flag for scripting mode "-s" just read the file executing each line as a command and then exit
	// TODO: Add an environment variable for the default file to read in case -f is not provided
	// TODO: Allow passing commands as arguments instead of just through stdin
	char *prog_name = argv[0];
	char *filename = NULL;
	FILE *file = NULL;
	char c;
	// Parse arguments
outer_loop:
	while (--argc > 0 &&
		   *(++argv)[0] == '-') { // starts from the second argument
		while (((c = *++argv[0]) != '\0')) {
			switch (c) {
			case 'h':
				printf(usage, prog_name);
				printf(
					"  -h            print this help and exit\n"
					"  -S, --scratch don't write changes to the file\n"
					"  -f            the following argument is a file name\n"
					"  -s            scripting mode, execute the file as a script; "
					"                don't write changes to the file and don't start interactive mode\n");
				exit(0);
				break;
			case 'S':
				mode |= SCRATCH_MODE_FLAG;
				break;
			case 's':
				mode &= SCRIPT_MODE_FLAG;
				break;
			case 'f':
				if (argc <= 1) { // we are in the last argument
					fprintf(
						stderr,
						"Missing required argument after -f option <filename>.\n");
					fprintf(stderr, usage, prog_name);
					exit(1);
				}
				// the next argument after 'f' is the name of the file
				// since we exit in case we don't have more arguments, argv+1 always exists
				filename = *(argv + 1);
				file = fopen(filename, "r");
				if (file == NULL) {
					fprintf(stderr, "%s: Error opening '%s': %s\n", prog_name,
							filename, strerror(errno));
					fprintf(stderr, usage, prog_name);
					exit(1);
				}
				load_file(file);
				fclose(file);
				break;
			case '-':
				if (*++argv[0] == '\0') {
					// Stop parsing options
					// All other arguments are passed to the program as commands
					goto end;
				}
				if (strcmp("scratch", argv[0]) == 0) {
					mode |= SCRATCH_MODE_FLAG;
					// next argument
					goto outer_loop;
				}
				fprintf(stderr, "%s: ERROR: Unrecognized long option '%s'\n",
						prog_name, argv[0]);
				fprintf(stderr, usage, prog_name);
				exit(1);
				break;
			default:
				fprintf(stderr, "%s: ERROR: Unrecognized option '%c'\n",
						prog_name, c);
				exit(1);
				break;
			}
		}
	}
end:

	// TODO: add an option for colors !isatty should not use ansi codes by default --color always to force it
	if (isatty(STDOUT_FILENO)) {
		printf("Welcome to your scheduler!\n");
		printf("Type 'help' for usage information\n");
	}
	char *line = NULL, *args = NULL;
	char *prompt = "⮚";
	unsigned long n = 0;
	int nread = 0;
	int args_len;
	char formatted_time[30];
	char start_date[DATE_STR_SIZE];
	char end_date[DATE_STR_SIZE];
	int date_len;
	// DONE: separate this into two different functions one to load the file and another one for interactive use
	// DONE: Think about using the prompt to present the current state
	// It should be clear that a task has been stated maybe displaying the name of the task in the prompt or just
	// "ongoing > "
	while (mode & INTERACTIVE_MODE_FLAG) {
		if (isatty(STDOUT_FILENO)) {
			if (tasks[i].start.tm_mday == 0) {
				printf("%s ", prompt);
			} else {
				printf("ongoing %s ", prompt);
			}
			fflush(stdout);
		}
		nread = getline(&line, &n, stdin);
		if (nread == -1)
			break;
		line[nread - 1] = '\0';
		char *first_word = strtok(line, " ");
		if (first_word == NULL)
			continue;
		size_t first_word_len = strlen(first_word);
		args_len = nread - first_word_len - 2;
		args = line + first_word_len + 1;

		if (strcmp(first_word, "start") == 0) {
			if (tasks[i].start.tm_mday != 0) {
				printf(
					"Warning: there is a task currently open. Ignoring overriding\n");
				// TODO: maybe ask to confirm before overriding
				continue;
			}
			if (args_len > 0) {
				char *res = str_to_tm(&tasks[i].start, args);
				if (res == NULL) {
					printf("Cannot parse %s as a datetime\n", args);
				}
			} else {
				now = time(NULL);
				tasks[i].start = *localtime(&now);
				if (filename && !(mode & SCRATCH_MODE_FLAG)) {
					date_len = strftime(start_date, DATE_STR_SIZE, time_format,
										&tasks[i].start);
					if (date_len == 0) {
						fprintf(stderr, "%d is not enough space\n",
								DATE_STR_SIZE);
						exit(1);
					}
					write_to_file(filename, "start", start_date);
				}
			}
		} else if (strcmp(first_word, "end") == 0) {
			if (tasks[i].start.tm_mday == 0) {
				printf("Warning: ignoring end. No open tasks\n");
				continue;
			}
			if (args_len > 0) {
				char *res = str_to_tm(&tasks[i].end, args);
				if (res == NULL) {
					printf("Cannot parse %s as a datetime\n", args);
				}
			} else {
				now = time(NULL);
				tasks[i].end = *localtime(&now);
			}
			time_t start = mktime_or_exit(&tasks[i].start);
			time_t end = mktime_or_exit(&tasks[i].end);
			tasks[i].time = end - start;
			if (filename && !(mode & SCRATCH_MODE_FLAG)) {
				date_len = strftime(end_date, DATE_STR_SIZE, time_format,
									&tasks[i].end);
				if (date_len == 0) {
					fprintf(stderr, "%d is not enough space\n", DATE_STR_SIZE);
					exit(1);
				}
				write_to_file(filename, "end", end_date);
			}
			i++;
		} else if (strcmp(first_word, "quit") == 0) {
			break;
		} else if (strcmp(first_word, "todo") == 0) {
			// TODO: the first argument should be the name of the task
			// todo list [issue] # lists all the todos filtered optionally by issue
			// todo add <issue> summary
			// todo complete <todo ID> # registers the time it was completed and
			// the args are the name of the argument. Try to open a file with the same name under ~/Work/issues and just cat it here.
			char *next_word = strtok(NULL, " ");
			if (strcmp(next_word, "add") == 0) {
				char *todo_desc = args + strlen(next_word) + 1;
				todos[todo_index].status = OPEN;
				todos[todo_index].description = checked_realloc(
					todos[todo_index].description, args_len + 1);
				strncpy(todos[todo_index].description, todo_desc,
						strlen(todo_desc) + 1);
				todo_index++;
				if (filename && !(mode & SCRATCH_MODE_FLAG)) {
					write_to_file(filename, "todo add",
								  todos[todo_index].description);
				}
			} else if (strcmp(next_word, "list") == 0) {
				for (int i = 0; i < todo_index; i++) {
					printf("%d: [%s] %s\n", i, status_to_str(todos[i].status),
						   todos[i].description);
				}
			} else if (strcmp(next_word, "complete") == 0) {
				next_word = strtok(NULL, "");
				char *endp = NULL;
				long cmpl_idx = strtol(next_word, &endp, 10);
				if (*next_word == '\0' || *endp != '\0') {
					printf("Invalid number '%s'\n", next_word);
					continue;
				}
				if (cmpl_idx < 0 || cmpl_idx > todo_index) {
					printf("Invalid todo index '%s'\n", next_word);
					continue;
				}
				todos[cmpl_idx].status = DONE;
				if (filename && !(mode & SCRATCH_MODE_FLAG)) {
					write_to_file(filename, "todo add",
								  todos[todo_index].description);
				}
			} else if (strcmp(next_word, "unfinish") == 0) {
				next_word = strtok(NULL, "");
				char *endp = NULL;
				long cmpl_idx = strtol(next_word, &endp, 10);
				if (*next_word == '\0' || *endp != '\0') {
					printf("Invalid number '%s'\n", next_word);
					continue;
				}
				todos[cmpl_idx].status = OPEN;
			} else {
				printf("Undefined action %s\n", next_word);
			}
		} else if (strcmp(first_word, "tasks") == 0) {
			// TODO: optimize memory allocations and clean up this code
			char *dir_name = "/home/augusto/Work/issues/";
			DIR *p_dir = opendir(dir_name);
			if (p_dir == NULL) {
				fprintf(stderr, "ERROR: opendir: %s\n", strerror(errno));
				exit(1);
			}
			struct dirent *p_dirent = NULL;
			while ((p_dirent = readdir(p_dir)) != NULL) {
				if (strcmp(p_dirent->d_name, "..") == 0 ||
					strcmp(p_dirent->d_name, ".") == 0)
					continue;
				char task_name[256];
				int k;
				char c;
				int diff;
				for (k = 0; k < 256 && (c = p_dirent->d_name[k]) != '\0'; k++) {
					diff = 'a' - 'A';
					task_name[k] = (c <= 'z' && c >= 'a') ? c - diff : c;
					if (task_name[k] == '.')
						break;
				}
				if (k < 256)
					task_name[k] = '\0';
				else
					task_name[255] = '\0';
				printf("* %s\n", task_name);
				char *file_name =
					malloc(strlen(dir_name) + strlen(p_dirent->d_name) + 1);
				sprintf(file_name, "%s%s", dir_name, p_dirent->d_name);
				FILE *f = fopen(file_name, "r");
				if (f == NULL) {
					fprintf(stderr, "ERROR: fopen: %s\n", strerror(errno));
				}
				char *desc = NULL;
				int nread = 0;
				size_t n = 0;
				nread = getline(&desc, &n, f);
				desc[nread - 1] = '\0';
				printf("    %s\n", desc + 2);
				free(desc);
				free(file_name);
				fclose(f);
			}
			closedir(p_dir);
			free(p_dirent);
		} else if (strcmp(first_word, "name") == 0 ||
				   strcmp(first_word, "n") == 0) {
			if (args_len > 0) {
				tasks[i].name = checked_realloc(tasks[i].name, args_len + 1);
				strncpy(tasks[i].name, args, args_len + 1);
				if (filename && !(mode & SCRATCH_MODE_FLAG)) {
					write_to_file(filename, "name", tasks[i].name);
				}
			} else {
				printf("command 'name': requires an argument\n");
			}
		} else if (strcmp(first_word, "d") == 0 ||
				   strcmp(first_word, "desc") == 0 ||
				   strcmp(first_word, "description") == 0) {
			if (args_len > 0) {
				tasks[i].description =
					checked_realloc(tasks[i].description, args_len + 1);
				strncpy(tasks[i].description, args, args_len + 1);
				if (filename && !(mode & SCRATCH_MODE_FLAG)) {
					write_to_file(filename, "description",
								  tasks[i].description);
				}
			} else {
				printf("command 'description': requires an argument\n");
			}
		} else if (strcmp(first_word, "today") == 0) {
			now = time(NULL);
			char formatted_time[30];
			struct tm today = *localtime(&now);
			int total_today = 0;
			char start_hour[6]; // HH:MM\0
			char end_hour[6]; // HH:MM\0
			bool first = true;
			for (int j = 0; j < i; j++) {
				if (tasks[j].start.tm_year == today.tm_year &&
					tasks[j].start.tm_yday == today.tm_yday) {
					sprintf(start_hour, "%02d:%02d", tasks[j].start.tm_hour,
							tasks[j].start.tm_min);
					sprintf(end_hour, "%02d:%02d", tasks[j].end.tm_hour,
							tasks[j].end.tm_min);
					duration(formatted_time, tasks[j].time);
					formatted_time[29] = '\0';
					if (!first) {
						printf("|\n");
					}
					printf(
						"* \033[32m%s\033[0m\n|\n| \033[1;34m%s\033[0m for \033[1;34m%s\033[0m: %s\n|\n* \033[31m%s\033[0m\n",
						start_hour, tasks[j].name, formatted_time,
						tasks[j].description, end_hour);
					total_today += tasks[j].time;
					first = false;
				}
			}
			char estimated_end_str[DATE_STR_SIZE];
			time_t estimated_end;
			time_t missing = 7 * 3600 - total_today;
			now = time(NULL);
			if (tasks[i].start.tm_mday != 0) {
				if (tasks[i].name != NULL)
					printf(
						"\033[1;36mYou are currently working on\033[0m: %s\n",
						tasks[i].name);
				if (tasks[i].description != NULL)
					printf("\t%s\n", tasks[i].description);

				total_today += now - mktime_or_exit(&tasks[i].start);
				missing = 7 * 3600 - total_today;
			}
			estimated_end = missing + now;
			duration(formatted_time, total_today);
			struct tm estimated_end_tm = *localtime(&estimated_end);
			int date_len = strftime(estimated_end_str, DATE_STR_SIZE,
									time_format, &estimated_end_tm);
			assert(date_len != 0);
			missing = (missing > 0) ? missing : 0;
			printf("You have \033[1mworked\033[22m for: \033[32m%s\033[39m\n",
				   formatted_time);
			if (missing > 0) {
				duration(formatted_time, missing);
				printf(
					"You are still \033[1mmissing\033[22m: \033[31m%s\033[39m\n",
					formatted_time);
				printf("Estimated endtime: \033[34m%s\033[39m\n",
					   estimated_end_str);
			} else {
				printf("\033[34m%s\033[39m\n", "All done for today!");
			}
		} else if (strcmp(first_word, "print") == 0) {
			for (int j = 0; j < i; j++) {
				duration(formatted_time, tasks[j].time);
				formatted_time[29] = '\0';
				printf("Task: %s\n\tTime: %s\n\t%s\n", tasks[j].name,
					   formatted_time, tasks[j].description);
			}
		} else if (strcmp(first_word, "debug") == 0) {
			printf("Implement debug\n");
		} else if (strcmp(first_word, "help") == 0) {
			printf(
				"Usage:\nstart: start a task\nend: to end the currently open "
				"task\ndescription: to describe the task\nname: to name the "
				"task\nhelp: to print this message\n");
		} else {
			// try to parse the input as a date
			int year, month, day, total = 0;
			int ret = sscanf(first_word, "%d-%d-%d", &year, &month, &day);
			char *cur_task = NULL;
			int subtotal_task = 0;
			size_t desc_counter = 0;
			char *descriptions[30];
			if (ret == 3) {
				printf("On %d-%02d-%02d (YYYY-MM-DD)\n", year, month, day);
				int j;
				for (j = 0; j < i; j++) {
					if (tasks[j].start.tm_mday == day &&
						tasks[j].start.tm_mon == month - 1 &&
						tasks[j].start.tm_year == year - 1900) {
						if (cur_task == NULL ||
							(tasks[i].name != NULL &&
							 strcmp(cur_task, tasks[j].name) != 0)) {
							duration(formatted_time, subtotal_task);
							formatted_time[29] = '\0';
							printf("\033[1;34mTask\033[0m: %s\n\tTime: %s\n",
								   cur_task, formatted_time);
							for (size_t k = 0; k < desc_counter; k++) {
								printf("\t* %s\n", descriptions[k]);
							}
							subtotal_task = 0;
							desc_counter = 0;
							cur_task = tasks[j].name;
						}
						subtotal_task += tasks[j].time;
						total += tasks[j].time;
						descriptions[desc_counter++] = tasks[j].description;
					}
				}
				if (cur_task != NULL) {
					duration(formatted_time, subtotal_task);
					formatted_time[29] = '\0';
					printf("\033[1;34mTask\033[0m: %s\n\tTime: %s\n", cur_task,
						   formatted_time);
					for (size_t k = 0; k < desc_counter; k++) {
						printf("\t* %s\n", descriptions[k]);
					}
				}
				duration(formatted_time, total);
				printf("%s worked\n", formatted_time);
			} else {
				printf("Unknown command: '%s'. Valid options: 'start' "
					   "'end' 'name' "
					   "'description' 'debug' 'print' and 'quit'\n",
					   line);
				continue;
			}
		}
	}
	if (isatty(STDOUT_FILENO))
		printf("\033[2K\r");

	for (int j = 0; j <= i; j++) {
		free(tasks[j].description);
		free(tasks[j].name);
	}
	free(line);
	return 0;
}
#endif

void load_file(FILE *file)
{
	time_t now, start, end;
	ssize_t nread, first_word_len;
	char *line = NULL, *args = NULL, *first_word;
	size_t args_len, n = 0;
	char formatted_time[30];
	struct tm today;
	while (1) {
		nread = getline(&line, &n, file);
		if (nread == -1)
			break;
		line[nread - 1] = '\0';
		first_word = strtok(line, " ");
		if (first_word == NULL)
			continue;
		first_word_len = strlen(first_word);
		args_len = nread - first_word_len - 2;
		args = line + first_word_len + 1;

		if (strcmp(first_word, "start") == 0) {
			if (tasks[i].start.tm_mday != 0) {
				printf(
					"Warning: there is a task currently open. Ignoring overriding\n");
				continue;
			}
			if (args_len > 0) {
				str_to_tm(&tasks[i].start, args);
			} else {
				now = time(NULL);
				tasks[i].start = *localtime(&now);
			}
		} else if (strcmp(first_word, "end") == 0) {
			if (tasks[i].start.tm_mday == 0) {
				printf("Warning: ignoring end. No open tasks\n");
				continue;
			}
			if (args_len > 0) {
				str_to_tm(&tasks[i].end, args);
			} else {
				now = time(NULL);
				tasks[i].end = *localtime(&now);
			}
			start = mktime_or_exit(&tasks[i].start);
			end = mktime_or_exit(&tasks[i].end);
			tasks[i].time = end - start;
			i++;
		} else if (strcmp(first_word, "quit") == 0) {
			break;
		} else if (strcmp(first_word, "name") == 0) {
			if (args_len > 0) {
				tasks[i].name = checked_realloc(tasks[i].name, args_len + 1);
				strncpy(tasks[i].name, args, args_len + 1);
			} else {
				printf(
					"command 'name': requires an argument with the description\n");
			}
		} else if (strcmp(first_word, "description") == 0) {
			if (args_len > 0) {
				tasks[i].description =
					checked_realloc(tasks[i].description, args_len + 1);
				strncpy(tasks[i].description, args, args_len + 1);
			} else {
				printf(
					"command 'description': requires an argument with the description\n");
			}
		} else if (strcmp(first_word, "today") == 0) {
			now = time(NULL);
			today = *localtime(&now);
			int total_today = 0;
			for (int j = 0; j < i; j++) {
				if (tasks[j].start.tm_year == today.tm_year &&
					tasks[j].start.tm_yday == today.tm_yday) {
					duration(formatted_time, tasks[j].time);
					formatted_time[29] = '\0';
					printf("\033[1;34mTask\033[0m: %s\n\tTime: %s\n\t%s\n",
						   tasks[j].name, formatted_time, tasks[j].description);
					total_today += tasks[j].time;
				}
			}
			char estimated_end_str[DATE_STR_SIZE];
			time_t estimated_end;
			time_t missing = 7 * 3600 - total_today;
			now = time(NULL);
			if (tasks[i].start.tm_mday != 0) {
				printf("You are currently working on: %s\n", tasks[i].name);
				total_today += now - mktime(&tasks[i].start);
				missing = 7 * 3600 - total_today;
			}
			estimated_end = missing + now;
			duration(formatted_time, total_today);
			struct tm estimated_end_tm = *localtime(&estimated_end);
			int date_len = strftime(estimated_end_str, DATE_STR_SIZE,
									time_format, &estimated_end_tm);
			assert(date_len != 0);
			missing = (missing > 0) ? missing : 0;
			printf("You have \033[1mworked\033[22m for: \033[32m%s\033[39m\n",
				   formatted_time);
			duration(formatted_time, missing);
			printf("You are still \033[1mmissing\033[22m: \033[31m%s\033[39m\n",
				   formatted_time);
			printf("Estimated endtime: \033[34m%s\033[39m\n",
				   estimated_end_str);
		} else if (strcmp(first_word, "print") == 0) {
			char formatted_time[30];
			for (int j = 0; j < i; j++) {
				duration(formatted_time, tasks[j].time);
				formatted_time[29] = '\0';
				printf("Task: %s\n\tTime: %s\n\t%s\n", tasks[j].name,
					   formatted_time, tasks[j].description);
			}
		} else if (strcmp(first_word, "debug") == 0) {
			printf("Implement debug\n");
		} else if (strcmp(first_word, "help") == 0) {
			printf(
				"Usage:\nstart: start a task\nend: to end the currently open "
				"task\ndescription: to describe the task\nname: to name the "
				"task\nhelp: to print this message\n");
		} else {
			printf("Unknown command: '%s'. Valid options: 'start' "
				   "'end' 'name' "
				   "'description' 'debug' 'print' and 'quit'\n",
				   line);
			continue;
		}
	}
	free(line);
}

// TODO: handle this error gracefully, by writing all the changes to the file before exiting
// Try to realloc. exit 1 if the the function fails to allocate
// If successful returns the reallocated memory
void *checked_realloc(void *p, size_t size)
{
	if (size == 0) {
		fprintf(stderr, "You can't call 'checked_realloc' with size = 0\n");
		exit(1);
	}
	p = realloc(p, size);
	if (p == NULL) {
		fprintf(stderr, "ERROR: malloc: could not allocate memory\n");
		exit(1);
	}
	return p;
}

/* Fill buf with a human readable duration string */
void duration(char *buf, time_t seconds)
{
	sprintf(buf, "%ldh %ldm %lds", seconds / 3600, (seconds % 3600) / 60,
			seconds % 60);
}

time_t mktime_or_exit(struct tm *t)
{
	time_t start = mktime(t);
	if (start == -1) {
		fprintf(stderr, "ERROR: time %ld\n", start);
	}
	return start;
}

void print_tm(struct tm *t)
{
	printf("{\n\ttm_sec: %d,\n\ttm_min\t: %d,\n\ttm_hour: %d,\n\ttm_mday: "
		   "%d,\n\ttm_mon: "
		   "%d,\n\ttm_year: %d\n\ttm_wday: %d\n\ttm_yday: %d\n\ttm_isdst: "
		   "%d\n}\n",
		   t->tm_sec, t->tm_min, t->tm_hour, t->tm_mday, t->tm_mon, t->tm_year,
		   t->tm_wday, t->tm_yday, t->tm_isdst);
}

char *str_to_tm(struct tm *t, char *str)
{
	memset(t, 0, sizeof(*t));
	char *res = strptime(str, time_format, t);
#ifdef DEBUG
	print_tm(t);
#endif
	// strptime doesn't set tm_isdst.
	// setting it to -1 means that the information is not available and mktime will try to guess
	t->tm_isdst = -1;
	return res;
}

void write_to_file(const char *filename, char *command, char *args)
{
	FILE *stream = fopen(filename, "a");
	if (stream == NULL) {
		fprintf(stderr, "ERROR: fopen %s: %s\n", filename, strerror(errno));
		exit(1);
	}
	fprintf(stream, "%s %s\n", command, args);
	fflush(stream);
	fclose(stream);
}

char *status_to_str(int status)
{
	switch (status) {
	case OPEN:
		return "OPEN";
	case DONE:
		return "DONE";
	}
	return "";
}
