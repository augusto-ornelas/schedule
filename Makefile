FLAGS=-Wall -Wextra -g
all:
	$(CC) $(FLAGS) main.c -o schedule
.PHONY: test
test:
	$(CC) $(FLAGS) -DTEST main.c -o test
