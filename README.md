# Task manager and time tracker

## Workflow:

1. You start working
  Use the `start` command to start recording your activities
2. Check your To-Do's
  Use `todo list [task name]` to see all the To-Do's (optionally filtered by task)
3. (Optional to remind you what you're working on) Select a todo you want to work on
  Use `todo next <todo ID>` to select a todo as the next to complete
4. Once you are finished mark the To-Do as completed
  Use `todo complete <todo ID`. This will add to your log an entry with the description of the To-Do and the time it was marked as finished
5. At any time you can stop recording your work
  Use `end` to stop recording your activities

## Stats:

- To see the progress of the current day
  Use `today` to list all the To-Dos completed today, grouped by task. This will include the amount of hours worked per task and the total per day
- To see the summary of a given date
  You can just enter the date in the format YYYY-MM-DD. This will present the same information as `today`.
